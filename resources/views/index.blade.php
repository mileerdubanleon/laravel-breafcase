<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Mileer león</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="routeName" content="{{ Route::currentRouteName() }}">
    {{-- favicon --}}
	<link rel="icon" type="image/x-icon" href="/static/images/icon.png">
	{{-- bootstrap 5.3.2 --}}
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    {{-- CSS Styles --}}
    <link href="{{ asset('static/css/styles.css') }}" rel="stylesheet">
</head>

	<body>
		{{-- home --}}
		@include('components.nav')
	</body>

    {{-- Script bootstrap 5.3.2 --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    {{-- Script --}}
    <script src="{{ asset('static/js/scripts.js') }}"></script><!-- Custom scripts -->

</html>
