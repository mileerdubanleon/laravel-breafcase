<div class="container" bis_skin_checked="1">
    <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
        <div class="col-md-2 mb-2 mb-md-0" bis_skin_checked="1">
            <a href="/" class="d-inline-flex link-body-emphasis text-decoration-none">
                img
            </a>
        </div>
    
        <ul class="nav col-6 col-md-auto mb-2 justify-content-center mb-md-0">
            <li><a href="#" class="nav-link px-2 link-secondary">Inicio</a></li>
            <li><a href="#" class="nav-link px-2 link-secondary">Quien soy</a></li>
            <li><a href="#" class="nav-link px-2 link-secondary">Servicios</a></li>
            <li><a href="#" class="nav-link px-2 link-secondary">Mis proyectos</a></li>
            <li><a href="#" class="nav-link px-2 link-secondary">Tienda</a></li>
        </ul>
        @if (Auth::check())
            <ul>
                <li class="nav-item dropdown" style="list-style:none;">
                    <a class="nav-link dropdown-toggle link-secondary" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <span class="theme-text link-secondary"> 
                            <span class="text-dark">{{ Auth::user()->name }}</span>
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="dropdown-item" href="#" data-theme="dark">Editar perfil</a>
                        </li>
                        @if (Auth::user()->role == 1 || Auth::user()->role == 2)
                            <li>
                                <a class="dropdown-item" href="{{ url('/admin') }}" data-theme="light">Administración</a>
                            </li>
                        @endif
                    </ul>
                </li>
            </ul>
            @else:
            <div class="col-md-3 text-end" bis_skin_checked="1">
                <a type="button" class="btn btn-outline-dark me-2" href="{{ url('/login') }}">Inicia sesión</a>
                <a type="button" class="btn btn-dark" href="{{ url('/register') }}">Regístrate</a>
            </div>
        @endif
    
        {{-- color background --}}
        <ul>
            <li class="nav-item dropdown" style="list-style:none;">
            <a class="nav-link dropdown-toggle link-secondary" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <span class="theme-text link-secondary">Cambiar Tema</span>
            </a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#" data-theme="dark">Dark</a></li>
                <li><a class="dropdown-item" href="#" data-theme="light">Light</a></li>
            </ul>
            </li>
        </ul>
    </header>
</div>