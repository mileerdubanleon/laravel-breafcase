@extends('admin.master')
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<main id="main" class="main">
	<div class="">
        <h1>Editar usuario</h1>
        <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/admin/users/all') }}">Usuarios</a></li>
			<li class="breadcrumb-item active">Editar usuario: <em>{{ $u->name }}</em> </li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="container">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body mt-3 col-md-12">

				<div class="p-3 row">
					<div class="col-md-6 mb-5">
						<div class="row text-align-center">
							<div class="">
								<h2 class="title">Información Usuario</h2>
							</div>
			
							<div class="container">
								{{-- @if(is_null($u->avatar))
									<img src="{{ url('/static/images/default-avatar.png') }}" class="avatar">
								@else
									<img src="{{ url('/uploads_users/'.$u->id.'/'.$u->avatar) }}" class="avatar">
								@endif --}}
	
								<div class="row">
									<div class="d-flex gap-2">
										<span class="font-weight-bold"> Nombre:</span>
										<span class="text">{{ $u->name }} {{ $u->lastname }} </span>
									</div>
									<div class="d-flex gap-2">
										<span class="font-weight-bold"> Estado del usuario:</span>
										<span class="text">{{ getUserStatusArray(null,$u->status) }}</span>
									</div>
									<div class="d-flex gap-2">
										<span class="font-weight-bold">Correo electrónico:</span>
										<span class="text">{{ $u->email }}</span>
									</div>
									<span class="title"><i class="fas fa-calendar-alt"></i> Fecha de registro:</span>
									<span class="text">{{ $u->created_at }}</span>
									<span class="title"><i class="fas fa-user-shield"></i> Role de usuario:</span>
									<span class="text">{{ getRoleUserArray(null,$u->role) }}</span>
								</div>
								<div class="my-3">
									@if(kvfj(Auth::user()->permissions, 'user_banned'))
										@if($u->status  == "100")
											<a href="{{ url('/admin/user/'.$u->id.'/banned') }}" class="btn btn-success">Activar Usuario</a>
										@else
											<a href="{{ url('/admin/user/'.$u->id.'/banned') }}" class="btn btn-danger">Suspender Usuario</a>
										@endif
									@endif
								</div>
							</div>
						</div>
					</div>
			
					<div class="col-md-6">
						<div class="">
							<div class="d-flex justify-content-center">
								<h2> Editar Información</h2>
							</div>
	
								{!! Form::open(['url' => '/admin/user/'.$u->id.'/edit']) !!}
									<div class="row">
					
										<div class="col-md-6">
												<label class="my-3">Tipo de usuario:</label>
											<div class="input-group">
													<span class="input-group-text" id="basic-addon1">
													</span>
												{!! Form::select('user_type', getRoleUserArray('list', null), $u->role, ['class' => 'form-control']) !!}
											</div>
										</div>
					
									</div>
			
								<div class="row my-3">
									<div class="col-md-12">
										{!!Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
									</div>
								</div>
								{!! Form::close() !!}
	
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</main>
<!-- jQuery y Bootstrap JS CDN -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
{{-- Footer adminlte --}}
