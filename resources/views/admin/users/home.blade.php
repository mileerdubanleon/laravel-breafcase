@extends('admin.master')
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- DataTables CSS CDN -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
<main id="main" class="main">
    <div class="container">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body mt-3">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="font-weight-bold">
                            <td>ID</td>
                            {{-- <td></td> --}}
                            <td>Nombre</td>
                            <td>Apellido</td>
                            <td>Email</td>
                            <td>Role</td>
                            <td>Estado</td>
                            <td>Accion</td>
                        </tr>
                        </tr>
                    </thead>
                <tbody>
                    @foreach($users as $user)
					<tr>
						<td>{{ $user->id }}</td>
						{{-- <td width="48">
							@if(is_null($user->avatar))
		       		     	<img src="{{ url('/static/images/default-avatar.png') }}" class="img-fluid rounded-circle">
		       		     	@else
		       		     	<img src="{{ url('/uploads_users/'.$user->id.'/'.$user->avatar) }}" class="img-fluid rounded-circle">
		       		     	@endif
						</td> --}}
						<td>{{ $user->name }}</td>
						<td>{{ $user->lastname }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ getRoleUserArray(null,$user->role) }}</td>
						<td>{{ getUserStatusArray(null,$user->status) }}</td>
						<td>
							<div class="">
                                @if(kvfj(Auth::user()->permissions, 'user_edit'))
                                    <a href="{{ url('/admin/user/'.$user->id.'/edit') }}" class="btn btn-outline-success" data-toggle="tooltip" data-placement="top" title="Editar">
                                        Editar
                                    </a>
                                @endif
                                @if(kvfj(Auth::user()->permissions, 'user_permissions'))
                                    @if($user->role == 0)
                                        @else
                                        <a href="{{ url('/admin/user/'.$user->id.'/permissions') }}" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="Permisos de usuario">
                                            Permisos
                                        </a>
                                    @endif
                                @endif
							</div>
						</td>
					</tr>
					@endforeach
                </tbody>
                    <tfoot>
                        <tr class="font-weight-bold">
                            <td>ID</td>
                            {{-- <td></td> --}}
                            <td>Nombre</td>
                            <td>Apellido</td>
                            <td>Email</td>
                            <td>Role</td>
                            <td>Estado</td>
                            <td>Accion</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</main><!-- End #main -->
<!-- jQuery y Bootstrap JS CDN -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
{{-- Footer adminlte --}}

{{-- dataTables archivo de dependencias --}}
@include('admin.components.datatable')