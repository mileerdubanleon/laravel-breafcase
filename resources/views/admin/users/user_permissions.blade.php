@extends('admin.master')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<main id="main" class="main">
	<div class="">
        <h1>Editar usuario</h1>
        <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/admin/users/all') }}">Usuarios</a></li>
			<li class="breadcrumb-item active">Editar usuario: <em>{{ $u->name }}</em> </li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
	<div class="container-fluid">
		<div class="page_user">
			<form action="{{ url('/admin/user/'.$u->id.'/permissions') }}" method="POST">
				@csrf
		
				<div class="row">
					@foreach(user_permissions() as $key => $value)
						<div class="col-md-4">
							<div class="panel p-4 shadow">
								<div class="">
									<h2 class="title">{!! $value['icon'] !!} {{ $value['title'] }}</h2>
								</div>
								<div class="inside">
									@foreach($value['keys'] as $k => $v)
										<div class="form-check">
											<input class="form-check-input" name="{{ $k }}" type="checkbox" value="true" id="flexCheckDefault" @if(kvfj($u->permissions, $k)) checked @endif>
											<label class="form-check-label" for="flexCheckDefault">{{ $v }}</label>
										</div>
									@endforeach
								</div>
							</div>
						</div>
					@endforeach
				</div>
		
				<div class="row">
					<div class="col-md-12">
						<div class="panel my-4">
							<div class="inside">
							<input type="submit" value="Guardar" class="btn btn-primary">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>

