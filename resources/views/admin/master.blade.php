<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="routeName" content="{{ Route::currentRouteName() }}">
    <title>Administración</title>

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('static/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <!-- Vendor CSS Files -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet">
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="https://cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

    {{-- style --}}
    <!-- Template Main CSS File -->
    <link href="{{ asset('static/css/styleNav.css') }}" rel="stylesheet">
    {{-- preloader --}}
    <link href="{{ asset('static/css/preloader.css') }}" rel="stylesheet">
</head>
<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!-- end of preloader -->

    <div class="content" style="display: none;">
        {{-- Nav adminlte --}}
        @include('admin.components.nav')

        {{-- Sidebar adminlte --}}
        @include('admin.components.sidebar')

        {{-- Footer adminlte --}}
        @include('admin.components.footer')
    </div>
    


    <!-- Template Main JS File -->
    <script src="{{ asset('static/js/scriptNav.js') }}"></script><!-- Custom scripts -->
    {{-- preloader --}}
    
    <script src="{{ asset('static/js/preloader.js') }}"></script><!-- Custom scripts -->

</body>
</html>