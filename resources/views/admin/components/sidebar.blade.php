<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">
      @if(kvfj(Auth::user()->permissions, 'dashboard'))
        <li class="nav-item">
          <a href="{{ url('admin') }}" class="nav-link lk-dashboard">
            <i class="bi bi-grid"></i>
            <span>Dashboard</span>
          </a>
        </li><!-- End Dashboard Nav -->
      @endif

      @if(kvfj(Auth::user()->permissions, 'user_list'))
        <li class="nav-item">
          <a class="nav-link lk-user_list " href="{{ url('/admin/users/all') }}">
            <i class="bi bi-people"></i>
            <span>Usuarios</span>
          </a>
        </li><!-- End Users Nav -->
      @endif

    </ul>

</aside><!-- End Sidebar-->

<script>
  
  // routes active
  document.addEventListener('DOMContentLoaded', function() {
      let route = document.querySelector('meta[name="routeName"]').getAttribute('content');
      let menuLink = document.querySelector('.lk-' + route);
      
      if (menuLink) {
          menuLink.classList.add('collapsed');
      }
  });
</script>