@extends('connect.master')
@section('title', 'Register')
@section('content')
<!--Alert--->
@if(Session::has('message'))
		<div class="position-absolute d-flex justify-content-center text-align-center w-100">
			<div class="mtop16 alert alert-{{ Session::get('typealert') }}" style="display:none;">
				{{ Session::get('message') }}
				@if ($errors->any())
				<ul>
					@foreach($errors->all() as $error)
					<li> {{ $error }} </li>
					@endforeach
				</ul>
				@endif
				<script>
					$('.alert').slideDown();
					setTimeout(function(){ $('.alert').slideUp(); }, 10000)
				</script>
			</div>
		</div>
@endif

<div class="container_login d-flex justify-content-center">
	<div class="login-content">
		{!! Form::open(['url' => '/register']) !!}
			@csrf
			<img src="{{ asset('../static/img/logo.png') }}" class="my-2" alt="alternative">

			<h2 class="title">Registrate</h2>
			<div class="d-flex form-register">
				<div class="input-div one col-md-6">
					<div class="i">
						<i class="fas fa-pencil-alt"></i>
					</div>
					<div class="div">
						<h5>Nombre</h5>
						{!! Form::text('name', null, ['class' => 'input', 'required']) !!}
					</div>
				</div>
				<div class="input-div one col-md-6">
					<div class="i">
						<i class="fas fa-pencil-alt"></i>
					</div>
					<div class="div">
						<h5>Apellidos</h5>
						{!! Form::text('lastname', null, ['class' => 'input', 'required']) !!}
					</div>
				</div>
			</div>
			<div class="input-div one">
				<div class="i">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<div class="div">
					<h5>Correo</h5>
					{!! Form::email('email', null, ['class' => 'input', 'required']) !!}
				</div>
			</div>
			<div class="input-div one">
				<div class="i">
					<i class="fas fa-lock"></i>
				</div>
				<div class="div">
					<h5>Contraseña</h5>
					{!! Form::password('password', ['class' => 'input', 'required']) !!}
				</div>
			</div>
			<div class="input-div one">
				<div class="i">
					<i class="fas fa-lock"></i>
				</div>
				<div class="div">
					<h5>Repita su contraseña</h5>
					{!! Form::password('cpassword', ['class' => 'input', 'required']) !!}
				</div>
			</div>

			<div class="d-flex my-5 mx-3 gap-3">
				<a href="{{ url('/login') }}">Ya tienes una cuenta. Inicia sesión</a>
			</div>
			{!! Form::submit('Registrarse', ['class' => 'btn', 'value' => 'Login']) !!}


		{!! Form::close() !!}
	</div>
</div>

@stop