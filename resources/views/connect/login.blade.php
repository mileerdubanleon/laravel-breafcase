@extends('connect.master')
@section('title', 'login')
@section('content')
<!--Alert--->
@if(Session::has('message') || $errors->has('email'))
    <div class="position-absolute d-flex justify-content-center text-align-center w-100">
        <div class="mtop16 alert alert-{{ Session::get('typealert') }}" style="display:none;">
            @if(Session::has('message'))
                {{ Session::get('message') }}
            @endif

            @if($errors->has('email'))
                {{ $errors->first('email') }}
            @endif

            <script>
                $('.alert').slideDown();
                setTimeout(function(){ $('.alert').slideUp(); }, 10000)
            </script>
        </div>
    </div>
@endif

<div class="container_login d-flex justify-content-center">
	<div class="login-content row">
		{!! Form::open(['url' => '/login']) !!}

			@csrf
			<img src="{{ asset('../static/img/logo.png') }}" class="my-2" alt="alternative">

			<h2 class="title">Bienvenido</h2>
			<div class="input-div one">
				<div class="i">
					<i class="fas fa-user"></i>
				</div>
				<div class="div">
					<h5>Correo</h5>
					{!! Form::email('email', null, ['class' => 'input']) !!}
				</div>
			</div>
			<div class="input-div pass">
				<div class="i">
					<i class="fas fa-lock"></i>
				</div>
				<div class="div">
					<h5>Contraseña</h5>
					{!! Form::password('password', ['class' => 'input', 'id' => 'password-input']) !!}
				</div>
			</div>
			<div class="d-flex text-start my-4 options-sesion ">
				<a class="toggle-password col-md-12" onclick="togglePasswordVisibility()">Mostrar Contraseña</a>
			</div>
			<div class="d-flex text-start my-4 options-sesion">
				<a href="{{ url('/register') }}" class="col-md-6 d-flex ">Crea una cuenta </a>
				<a href="#" class="col-md-6 ">Recuperar contraseña</a>
			</div>
			{!! Form::submit('Ingresar', ['class' => 'btn', 'value' => 'Login']) !!}

		{!! Form::close() !!}
	</div>
</div>

@stop