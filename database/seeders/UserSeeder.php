<?php

namespace Database\Seeders;
use  App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Correo predeterminado
        User::create([
            'status' => '0',
            'role' => '1',
            'name' => 'Mileer',
            'lastname' => 'Leon',
            'email' => 'mileerdubanleon@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
    }
}
