const inputs = document.querySelectorAll(".input");

function addcl(){
	let parent = this.parentNode.parentNode;
	parent.classList.add("focus");
}

function remcl(){
	let parent = this.parentNode.parentNode;
	if(this.value == ""){
		parent.classList.remove("focus");
	}
}

inputs.forEach(input => {
	input.addEventListener("focus", addcl);
	input.addEventListener("blur", remcl);
});

/* Preloader */
$(window).on('load', function() {
    var preloaderFadeOutTime = 500;
    function hidePreloader() {
        var preloader = $('.spinner-wrapper');
        setTimeout(function() {
            preloader.fadeOut(preloaderFadeOutTime);
        }, 200);
    }
    hidePreloader();
});

// view password 
function togglePasswordVisibility() {
    var passwordInput = document.getElementById('password-input');
    var togglePasswordText = document.querySelector('.toggle-password');

    if (passwordInput.type === 'password') {
        passwordInput.type = 'text';
        togglePasswordText.textContent = 'Ocultar Contraseña';
    } else {
        passwordInput.type = 'password';
        togglePasswordText.textContent = 'Mostrar Contraseña';
    }
}