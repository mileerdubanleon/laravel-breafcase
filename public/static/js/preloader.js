document.addEventListener("DOMContentLoaded", function () {
    // Simula el tiempo de carga (puedes eliminar esto en un entorno de producción)
    setTimeout(function () {
        var preloader = document.querySelector(".preloader");
        var content = document.querySelector(".content");

        // Verifica si el preloader y el contenido existen
        if (preloader && content) {
            // Agrega la clase para ocultar gradualmente el preloader
            preloader.classList.add("hide-preloader");

            // Establece el contenido principal como visible
            content.style.display = "block";

            console.log("Preloader oculto, contenido visible");
        } else {
            console.error("No se encontraron el preloader o el contenido");
        }
    }, 2000);
});